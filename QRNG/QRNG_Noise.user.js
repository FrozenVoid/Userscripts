// ==UserScript==
// @name     QRNG_Noise
// @version  1
// @grant    none
// @description provide continuous random noise 
// @include https://qrng.anu.edu.au/random-bernoulli-noise/
// @include  https://qrng.anu.edu.au/random-white-noise/
// ==/UserScript==
var ctmax=179000;//min delay:3-6min
function setaloop(){
var caud=document.querySelector('audio');
if(caud){
caud.setAttribute("loop","loop");return;}
setTimeout(setaloop,1000);
}setTimeout(setaloop,1000);
  var thenum=new Uint32Array(1);
function truer(maxnum){
window.crypto.getRandomValues(thenum);
return thenum[0]%maxnum  
}

function ref(){
document.querySelector("#main button").click();
//console.log("CLK");
setTimeout(ref,ctmax+truer(ctmax))
}
setTimeout(ref,ctmax+truer(ctmax))