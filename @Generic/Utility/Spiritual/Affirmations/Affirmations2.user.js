// ==UserScript==
// @name     Affirmations2
// @version  1.02
// @description Show scrolling affirmation text to subconscious attention
// @grant    none
// @include *
// ==/UserScript==
var initdelay=11000;
var skip=1;//chars per scroll
var scrolldelay=(1000/1.45)|0;//1.45hz  scroll
var text="Its going to get better";//example

function drp(){if(document.hidden||window !== window.parent)return setTimeout(drp,scrolldelay);
a=document.getElementById('affirm1');
 if(!a){//div replacer
 var a=document.createElement('div');
 a.setAttribute("style","position:fixed;background:transparent;pointer-events: none;opacity:0.65;user-select:none;bottom:3em");
 a.setAttribute("id","affirm1");
 a.innerHTML="   "+text+"   ";
 document.body.appendChild(a);}

 a.innerHTML=a.innerHTML.substr(skip)+a.innerHTML.substr(0,skip);
setTimeout(drp,scrolldelay);
}setTimeout(drp,initdelay);
