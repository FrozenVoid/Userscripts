// ==UserScript==
// @name     RND_button
// @version  1.02
// @grant    unsafeWindow
// @description configurable random values from 0 to maxval(using Date.now())
// @include http*
// ==/UserScript==
var maxval=1;//1=0/1;
var default_val="?",init_sym="!";//placeholder strings 
var init_delay=7;//ms before displaying random value
var button_clear_at=3000;//ms delay before button default value reset;
var sleepdef=1000;
function gettrv(){return Date.now()%(1+maxval);}
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms || sleepdef));
}

var bt=document.createElement('button');
bt.addEventListener("click", function() {
this.innerHTML=init_sym;
sleep(init_delay).then(() => { this.innerHTML=gettrv()});
sleep(button_clear_at).then(() => { this.innerHTML=default_val });}); 
bt.innerHTML=default_val;
bt.style=`font-size:3vh;opacity:1;position:fixed;top:50vh;left:1vw;`;
unsafeWindow.document.body.appendChild(bt);

 